package ch.mobi.sa;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static ch.mobi.sa.TaskConstants.*;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class PredictionReporter {

    public static void reportSinglePrediction(Map<String, List<String>> lexicon) {
        List<Prediction> predictedTerms = identifyPredictedTerms(PREDICTION_OUT_PATH);

        resultOf("Seminar", predictedTermOf(SEMINAR_CLASS, predictedTerms)
                .map(term -> lexicon.getOrDefault(term, emptyList())).orElse(emptyList()));
        resultOf("Speaker", predictedTermOf(SPEAKER_CLASS, predictedTerms)
                .map(term -> lexicon.getOrDefault(term, emptyList())).orElse(emptyList()));
        resultOf("Location", predictedTermOf(LOCATION_CLASS, predictedTerms)
                .map(term -> lexicon.getOrDefault(term, emptyList())).orElse(emptyList()));
        resultOf("Start time", predictedTermOf(START_TIME_CLASS, predictedTerms)
                .map(term -> lexicon.getOrDefault(term, emptyList())).orElse(emptyList()));
        resultOf("End time", predictedTermOf(END_TIME_CLASS, predictedTerms)
                .map(term -> lexicon.getOrDefault(term, emptyList())).orElse(emptyList()));
    }

    private static void resultOf(String title, List<String> originTerms) {
        if (!originTerms.isEmpty()) {
            TaskLogger.cout(String.format("%s: %s", title, originTerms.get(0)));
        }
    }

    private static Optional<String> predictedTermOf(int expectedClass, List<Prediction> predictedTerms) {
        return predictedTerms.stream()
                .filter(prediction -> prediction.clazz.clazz == expectedClass)
                .sorted(Comparator.comparingDouble(p -> -p.getProbability())) // elects term with max probability
                .map(Prediction::getTerm)
                .findFirst();
    }

    private static List<Prediction> identifyPredictedTerms(Path path) {
        try (Stream<String> lines = Files.lines(path)) {
            return lines.map(line -> {
                String[] items = line.split(" ");
                return new Prediction(
                        items[5],
                        Stream.of(items[0], items[1], items[2], items[3], items[4])
                                .map(prediction -> prediction.split(":"))
                                .map(PredictedClass::new)
                                .max(Comparator.comparingDouble(p -> -p.getProbability())) // elects class with max probability
                                .get());
            }).collect(toList());
        } catch (IOException ioe) {
            TaskLogger.cout(ioe.getMessage());
        }
        return emptyList();
    }

    private static class Prediction {
        String term;
        PredictedClass clazz;

        public Prediction(String term, PredictedClass clazz) {
            this.term = term;
            this.clazz = clazz;
        }

        public String getTerm() {
            return term;
        }

        public Double getProbability() {
            return clazz.probability;
        }
    }

    private static class PredictedClass {
        Integer clazz;
        Double probability;

        public PredictedClass(String[] pair) {
            this.clazz = Integer.valueOf(pair[0]);
            this.probability = Double.valueOf(pair[1]);
        }

        public Double getProbability() {
            return probability;
        }
    }
}
