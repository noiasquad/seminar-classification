package ch.mobi.sa;

import java.io.*;
import java.nio.file.Path;

import static ch.mobi.sa.TaskConstants.*;
import static ch.mobi.sa.TaskLogger.cout;

public class VopalWabbit {

    public static String observation(String feature, int label) {
        return String.format("%d | %s", label, feature);
    }

    public static String sample(String sampleId, String feature) {
        return String.format("%s| %s", sampleId, feature);
    }

    public static void trainModel() {
        runSystemCall(String.format("vw --oaa %s %s -f %s --readable_model %s",
                CLASS_COUNT, TRAINING_SET_OUT_PATH, MODEL_INOUT_PATH, MODEL_TEXT_OUT_PATH));
    }

    public static void trainModel(int clazzCount, Path trainingSet, Path model, Path modelText) {
        runSystemCall(String.format("vw --oaa %s %s -f %s --readable_model %s",
                clazzCount, trainingSet, model, modelText));
    }

    static void testModel() {
        runSystemCall(String.format("vw -t -i %s %s --probabilities -p %s",
                MODEL_INOUT_PATH, TESTING_SET_OUT_PATH, PREDICTION_OUT_PATH));
    }

    static void testModel(Path model, Path testingSet, Path prediction) {
        runSystemCall(String.format("vw -t -i %s %s --probabilities -p %s", model, testingSet, prediction));
    }

    private static void runSystemCall(String command) {
        TaskLogger.cout(command);
        Runtime runtime = Runtime.getRuntime();
        try {
            Process process = runtime.exec(command);
            InputStream in = process.getInputStream();
            BufferedInputStream buf = new BufferedInputStream(in);
            InputStreamReader inread = new InputStreamReader(buf);
            BufferedReader bufferedreader = new BufferedReader(inread);

            // Read the command output
            String line;
            while ((line = bufferedreader.readLine()) != null) {
                cout(line);
            }
            // Check for command failure
            try {
                if (process.waitFor() != 0) {
                    cout("exit value = " + process.exitValue());
                }
            } catch (InterruptedException e) {
                cout(e.getMessage());
            } finally {
                // Close the InputStream
                bufferedreader.close();
                inread.close();
                buf.close();
                in.close();
            }
        } catch (IOException e) {
            cout(e.getMessage());
        }
    }
}
