package ch.mobi.sa;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static ch.mobi.sa.NgramGenerator.ngrams;
import static ch.mobi.sa.TaskConstants.*;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class DatasetBuilder {

    private static final Pattern PATTERN_PUNCTUATION_SYMBOLS = Pattern.compile("[\\.,;\"]");
    private static final Pattern PATTERN_LOCATION_TAG = Pattern.compile("<location>(.+?)</location>");
    private static final Pattern PATTERN_SPEAKER_TAG = Pattern.compile("<speaker>(.+?)</speaker>");
    private static final Pattern PATTERN_STIME_TAG = Pattern.compile("<stime>(.+?)</stime>");
    private static final Pattern PATTERN_ETIME_TAG = Pattern.compile("<etime>(.+?)</etime>");

    private static List<String> observations;
    private static Map<String, List<String>> lexicon;

    public static Map<String, List<String>> prepareTrainingSet() {
        processTaggedFiles(TAGGED_FILES_IN_PATH);
        saveTrainingSet();
        return lexicon;
    }

    private static void processTaggedFiles(Path dirPath) {
        observations = new ArrayList<>();
        lexicon = new HashMap<>();

        try (Stream<Path> files = Files.list(dirPath)) {
            files.sorted().forEach(path -> {
                String fileContent = linearizeFile(path);
                extractTaggedInfoFeatures(fileContent);
                extractUntaggedInfoFeatures(fileContent);
            });
        } catch (IOException ioe) {
            TaskLogger.cout(ioe.getMessage());
        }
    }

    private static void extractTaggedInfoFeatures(String text) {
        detectTag(text, PATTERN_SPEAKER_TAG)
                .forEach(content -> extractSpeakerFeatures(content)
                        .forEach(term -> observations.add(observation(term, SPEAKER_CLASS))));
        detectTag(text, PATTERN_LOCATION_TAG)
                .forEach(content -> extractLocationFeatures(content)
                        .forEach(term -> observations.add(observation(term, LOCATION_CLASS))));
        detectTag(text, PATTERN_STIME_TAG)
                .forEach(content -> extractTimeFeatures(content)
                        .forEach(term -> observations.add(observation(term, START_TIME_CLASS))));
        detectTag(text, PATTERN_ETIME_TAG)
                .forEach(content -> extractTimeFeatures(content)
                        .forEach(term -> observations.add(observation(term, END_TIME_CLASS))));
    }

    private static List<String> extractSpeakerFeatures(String text) {
        return extractTerms(text);
    }

    private static List<String> extractLocationFeatures(String text) {
        return extractTerms(text);
    }

    private static List<String> extractTimeFeatures(String text) {
        return extractTerms(text);
    }

    private static void extractUntaggedInfoFeatures(String text) {
        extractSeminarFeatures(dropTagDelimiters(text))
                .forEach(term -> observations.add(observation(term, SEMINAR_CLASS)));
    }

    private static List<String> extractSeminarFeatures(String text) {
        final String SEMINAR_TERM = "seminar";
        final int SEMINAR_LENGTH = SEMINAR_TERM.length();
        return extract23grams(text).stream()
                .filter(ngram -> {
                    int idx = ngram.toLowerCase().indexOf(SEMINAR_TERM);
                    return idx >= 0 && idx == ngram.length() - SEMINAR_LENGTH; // favour seminar at the end
                })
                .map(ngram -> feature(fillSpaces(ngram).toLowerCase(), ngram))
                .collect(toList());
    }

    private static List<String> extractTerms(String text) {
        return extract23grams(text).stream()
                .map(term -> feature(fillSpaces(term.toLowerCase()), text))
                .collect(toList());
    }

    private static List<String> extract23grams(String text) {
        return ngrams(minimizeSpaces(dropPunctuation(text)), 2, 3);
    }

    private static String feature(String term, String originalText) {
        if (lexicon.containsKey(term)) {
            lexicon.get(term).add(originalText);
        } else {
            List<String> list = new ArrayList<>();
            list.add(originalText);
            lexicon.put(term, list);
        }
        return term;
    }

    private static String observation(String feature, int label) {
        return VopalWabbit.observation(feature, label);
    }

    private static String dropTags(String text) {
        return text
                .replaceAll("<.+>.+</.+>", "")
                .replaceAll("<.+>", "")
                .replaceAll("</.+>", "");
    }

    private static String dropTagDelimiters(String text) {
        return text.replaceAll("<location>|</location>|<speaker>|</speaker>|<stime>|</stime>|<etime>|</etime>|<sentence>|</sentence>|<paragraph>|</paragraph>", "");
    }

    private static String fillSpaces(String text) {
        return text.replaceAll("\\s+", "_");
    }

    private static String minimizeSpaces(String text) {
        return text.replaceAll("\\s+", " ");
    }

    private static String dropPunctuation(String text) {
        return PATTERN_PUNCTUATION_SYMBOLS.matcher(text)
                .replaceAll("")
                .replaceAll(":", " ");
    }

    private static List<String> detectTag(String text, Pattern pattern) {
        List<String> occurences = new ArrayList<>();
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            occurences.add(dropTags(matcher.group(1)));
        }
        return occurences;
    }

    private static String linearizeFile(Path path) {
        TaskLogger.cout(path.toString());

        StringBuilder sb = new StringBuilder();
        try (Stream<String> lines = Files.lines(path)) {
            lines.forEach(s -> sb.append(s).append(" "));
        } catch (IOException ioe) {
            TaskLogger.cout(ioe.getMessage());
        }
        return sb.toString();
    }

    private static void saveTrainingSet() {
        try {
            Files.write(TRAINING_SET_OUT_PATH, observations, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
        } catch (IOException ioe) {
            TaskLogger.cout(ioe.getMessage());
        }
    }

    public static List<String> prepareSingleTestSample() {
        try (Stream<Path> files = Files.list(TEST_SAMPLE_IN_PATH)) {
            return files.map(path -> {
                String fileContent = dropTagDelimiters(linearizeFile(path));
                List<String> terms = extract23grams(fileContent).stream()
                        .map(String::toLowerCase)
                        .map(DatasetBuilder::fillSpaces)
                        .map(term -> VopalWabbit.sample(term, term))
                        .collect(toList());
                saveTestingSet(terms);
                return terms;
            }).findFirst().orElse(emptyList());
        } catch (IOException ioe) {
            TaskLogger.cout(ioe.getMessage());
        }
        return emptyList();
    }

    private static void saveTestingSet(List<String> terms) {
        try {
            Files.write(TESTING_SET_OUT_PATH, terms, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
        } catch (IOException ioe) {
            TaskLogger.cout(ioe.getMessage());
        }
    }
}
