package ch.mobi.sa;

import java.nio.file.Path;
import java.nio.file.Paths;

public interface TaskConstants {
    /*
        Path TAGGED_FILES_IN_PATH = Paths.get("./sa-tagged-test");
        Path TRAINING_SET_OUT_PATH = Paths.get("./sa-vw-test/sa-train.vw");
        Path TESTING_SET_OUT_PATH = Paths.get("./sa-vw-test/sa-test.vw");
        Path TEST_SAMPLE_IN_PATH = Paths.get("./sa-tagged-test");
        Path PREDICTION_OUT_PATH = Paths.get("./sa-vw-test/sa-predict.vw");
        Path MODEL_INOUT_PATH = Paths.get("./sa-vw-test/sa-model.vw");
    */
    Path TAGGED_FILES_IN_PATH = Paths.get("./sa-tagged");
    Path TRAINING_SET_OUT_PATH = Paths.get("./sa-vw/sa-train.vw");
    Path TESTING_SET_OUT_PATH = Paths.get("./sa-vw/sa-test.vw");
    Path TEST_SAMPLE_IN_PATH = Paths.get("./sa-tagged-test");
    Path PREDICTION_OUT_PATH = Paths.get("./sa-vw/sa-predict.vw");
    Path MODEL_INOUT_PATH = Paths.get("./sa-vw/sa-model.vw");
    Path MODEL_TEXT_OUT_PATH = Paths.get("./sa-vw/sa-model.txt");

    int SEMINAR_CLASS = 1;
    int LOCATION_CLASS = 2;
    int SPEAKER_CLASS = 3;
    int START_TIME_CLASS = 4;
    int END_TIME_CLASS = 5;
    int CLASS_COUNT = 5;
}
