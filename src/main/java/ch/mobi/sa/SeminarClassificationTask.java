package ch.mobi.sa;

import java.util.List;
import java.util.Map;

public class SeminarClassificationTask {

    public static void main(String... args) {
        Map<String, List<String>> lexicon = DatasetBuilder.prepareTrainingSet();
        VopalWabbit.trainModel();
        DatasetBuilder.prepareSingleTestSample();
        VopalWabbit.testModel();
        PredictionReporter.reportSinglePrediction(lexicon);
    }
}
