package ch.mobi.sa;

import java.nio.file.Path;
import java.nio.file.Paths;

public class VopalWabbitTest {

    private static final Path TRAINING_SET_PATH = Paths.get("./vw-test/train.vw");
    private static final Path TESTING_SET_PATH = Paths.get("./vw-test/test.vw");
    private static final Path PREDICTION_PATH = Paths.get("./vw-test/predict.vw");
    private static final Path MODEL_PATH = Paths.get("./vw-test/model.vw");
    private static final Path MODEL_TEXT_PATH = Paths.get("./vw-test/model.text");

    public static void main(String[] args) {
        VopalWabbit.trainModel(3, TRAINING_SET_PATH, MODEL_PATH, MODEL_TEXT_PATH);
        VopalWabbit.testModel(MODEL_PATH, TESTING_SET_PATH, PREDICTION_PATH);
    }
}
