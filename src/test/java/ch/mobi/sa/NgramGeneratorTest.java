package ch.mobi.sa;

public class NgramGeneratorTest {

    public static void main(String[] args) {
        for (String ngram : NgramGenerator.ngrams("<0.26.4.95.11.09.31.hf08+@andrew.cmu.edu.0>" +
                    " Type: cmu.andrew.academic.bio" +
                    " Topic: MHC Class II: A Target for Specific Immunomodulation of the" +
                    " Immune Response" +
                    " Dates: 3-May-95" +
                    " Time: 3:30 PM" +
                    " Place: Mellon Institute Conference Room" +
                    " PostedBy: Helena R. Frey on 26-Apr-95 at 11:09 from andrew.cmu.edu" +
                " Abstract: ", 1, 2, 3)) {
                System.out.println(ngram);
        }
    }
}
