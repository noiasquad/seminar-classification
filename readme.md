## Seminar announcement classification task

Given a corpus of seminar announcement documents: to develop a machine learning system able to recognize a predefined
set of attributes located inside every document. These attributes are the following:

* seminar
* location
* speaker
* start time
* end time

The corpus of documents and a description of the task are available here:

* see [Page model by Dayne Freitag](http://www.cs.cmu.edu/~dayne/SeminarAnnouncements/Model.html)
* see [Corpus of documents](http://www.cs.cmu.edu/~dayne/SeminarAnnouncements/AllDocuments.tar.gz)

The training set is composed of the given corpus of documents. Each document contains the attributes emphasized 
using SGML-tags.

## Algorithm

The hard work is the construction of the training dataset, which contains the observations, composed by the features 
and the labels. This dataset has to be designed in a way the machine algorithm can do its classification task.

The main idea is to develop a language based on well defined terms understood by the machine. These terms are
the features, and each term is classified in a class. The classes are in fact the elements of the enumeration of the 
attributes. It is a multi-class classification task.

The algorithm has to learn by example, in a supervised way, given the observed feature terms derived from the information
extracted from the corpus of documents, and their classification.

Given a new document added into the corpus, it has to use its knowledge of the features that characterizes the 
information in order to detect and classify the information of the document into the different attribute classes.

As a last step, the classified featured terms are visited and an probability-based election is done to select the more
common feature term for a given class, and a lexicon built during the training phase helps in retrieving the original
terms from the elected feature terms.

### Feature extraction

The extraction of the features explores the bi-grams and the 3-grams inside the text of each document, after some
cleaning done on the text, in order to normalize and standardize the extracted n-grams. 

### Training dataset

The whole set of n-grams issued from all the documents forms the set of features. For each feature term an observation 
is deposed into the training dataset.

A lexicon is built in parallel which associates a standardized feature term with a set of original terms. 

### Prediction

An input document has to be processed a similar way as a document used for building the training set. In fact, a subset
of the same documents can be used, after having removed the tags used to emphasize the expected attributes. As a result
a new set of terms is built using the same technique as for the training set.

The algorithm is responsible to classify the given terms. Each term is given a class with a probability, which
should helps in determining which term has the highest predicted probability inside a class.

If the elected term is present inside the lexicon, it is possible to retrieve a selection of the original terms
associated to this feature term as an answer to this classification task.

## Technical

- The machine learning algorithm is supported by the [Vopal Wabbit](https://github.com/JohnLangford/vowpal_wabbit) machine
learning system.
- The pipeline is supported by the main class ch.mobi.sa.SeminarClassificationTask

## Prerequisites

- The [Vopal Wabbit](https://github.com/JohnLangford/vowpal_wabbit) has to be installed separately and be available on the 
command line using the **vw** command.

## References

- [Vopal Wabbit a cutting edge machine learning system](https://github.com/JohnLangford/vowpal_wabbit)
- [Vopal Wabbit tutorial](https://github.com/JohnLangford/vowpal_wabbit/wiki/Tutorial)
- [Predicting probabilities with Vopal Wabbit](https://github.com/JohnLangford/vowpal_wabbit/wiki/Predicting-probabilities)






